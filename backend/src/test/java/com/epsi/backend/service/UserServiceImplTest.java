package com.epsi.backend.service;

import com.epsi.backend.model.User;
import com.epsi.backend.repository.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.*;

import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

class UserServiceImplTest {

    @Mock
    UserRepository userRepository;

    @InjectMocks
    UserServiceImpl userService;

    private User testUser;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        this.testUser = getTestUser();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void findAll() {
        //ARRANGE
        Long id = 1L;
        when(userRepository.findAll()).thenReturn(getTestUsers());
        //ACT
        List<User> users = (List<User>) userService.findAll();
        //ASSERT
        verify(userRepository, Mockito.times(1)).findAll();
    }

    @Test
    void findById() {
        Long id = 1L;
        when(userRepository.findById(id)).thenReturn(Optional.of(testUser));
        //ACT
        Optional user = userService.findById(id);
        //ASSERT
        verify(userRepository, Mockito.times(1)).findById(any());
    }

    @Test
    void findAllByFirstName() {
        String firstName = "Ella";
        when(userRepository.findAllByFirstName(firstName)).thenReturn(getElla());
        //ACT
        List<User> users = userService.findAllByFirstName(firstName);
        //ASSERT
        verify(userRepository, Mockito.times(1)).findAllByFirstName(any());
    }


    private User getTestUser() {
        User user = new User();
        user.setFirstName("Ella");
        user.setLastName("Fitzgerald");
        user.setAccountId("TestAccountId");
        return user;
    }

    private User getTestUser2() {
        User user = new User();
        user.setFirstName("Julie");
        user.setLastName("London");
        user.setAccountId("TestAccountId2");
        return user;
    }

    private List<User> getTestUsers(){
        List<User> list = new ArrayList<>();
        list.add(getTestUser());
        list.add(getTestUser2());
        return list;
    }

    private List<User> getElla(){
        List<User> list = new ArrayList<>();
        list.add(getTestUser());
        return list;
    }
}
