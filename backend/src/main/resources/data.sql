DROP TABLE IF EXISTS user;

CREATE TABLE user (
                              id INT AUTO_INCREMENT  PRIMARY KEY,
                              firstName VARCHAR(250) NOT NULL,
                              lastName VARCHAR(250) NOT NULL,
                              accountId VARCHAR(250) DEFAULT NULL
);

INSERT INTO user (firstName, lastName, accountId) VALUES
('Ella', 'Fiitzgerald', 'ACCOUNT1'),
('Duke', 'Ellington', 'ACCOUNT2'),
('Julie', 'London', 'ACCOUNT3');
