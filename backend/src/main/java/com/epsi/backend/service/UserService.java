package com.epsi.backend.service;

import com.epsi.backend.model.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    Iterable<User> findAll();

    Optional<User> findById(long id);

    List<User> findAllByFirstName(String firstName);
}
