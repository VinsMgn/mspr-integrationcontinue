package com.epsi.backend.service;

import com.epsi.backend.controller.UserController;
import com.epsi.backend.model.User;
import com.epsi.backend.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class UserServiceImpl implements UserService {

    private static final Logger LOG = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository userRepository;

    public Iterable<User> findAll(){
        Iterable<User> users =  userRepository.findAll();
        LOG.info(users.toString());
        return users;
    }

    @Override
    public Optional<User> findById(long id) {
       return userRepository.findById(id);
    }

    @Override
    public List<User> findAllByFirstName(String firstName) {
        return userRepository.findAllByFirstName(firstName);
    }
}
