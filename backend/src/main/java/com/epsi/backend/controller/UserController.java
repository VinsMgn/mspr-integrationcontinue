package com.epsi.backend.controller;

import com.epsi.backend.model.User;
import com.epsi.backend.service.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Optional;

//http://localhost:8080/swagger-ui/

@Controller
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserServiceImpl userService;

    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);
    /**
     * Fake route asking for some User H2 DB
     *
     * @return
     */
    @GetMapping(value="/", produces = "application/json")
    public @ResponseBody
    Iterable<User> getAllUser() {
        LOG.info("All Users have been asked");
        Iterable<User> users = userService.findAll();
        LOG.info(users.toString());
        return users;
    }
    /**
     * Fake route asking for some User H2 DB
     *
     * @param id
     * @return
     */
    @GetMapping(value="/{id}", produces = "application/json")
    public @ResponseBody
    Optional<User> getUser(@PathVariable long id) {
        LOG.info("User has been asked");
        return userService.findById(id);
    }

    /**
     * Fake route asking for some User H2 DB
     *
     * @param firstName
     * @return
     */
    @GetMapping(value="/byFirstName/{firstName}", produces = "application/json")
    public @ResponseBody
    List<User> findAllByFirstName(@PathVariable String firstName) {
        LOG.info(String.format("User has been asked by FirstName : %s", firstName));
        return userService.findAllByFirstName(firstName);
    }

}
