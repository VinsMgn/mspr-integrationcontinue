package com.epsi.backend.model;

import javax.persistence.*;

/**
 * A fake class to return by the API
 */
@Entity
public class User {
    @Id
    @GeneratedValue
    @Column(name = "Id", nullable = false)
    private Long id;

    @Column(name = "lastname", nullable = false)
    private String lastName;
    @Column(name = "firstname", nullable = false)
    private String firstName;
    @Column(name = "accountid", nullable = false)
    private String accountId;

    public User(Long id, String lastName, String firstName, String accountId) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.accountId = accountId;
    }

    public User() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", accountId='" + accountId + '\'' +
                '}';
    }
}
