# Backend

## Steps to Setup

**1. Build and run the app using maven**

```bash
mvn package
java -jar target/XXX-1.0.0.jar
```

Alternatively, you can run the app without packaging it using -

```bash
mvn spring-boot:run
```

The app will start running at <http://localhost:8080>.

# TODO

- Procédure installation et exploitation application
- Documentation de l'application
- Infra de suivi et de résolution des incidents (ticketing gitlab - accès client)
- Système gestion des informations de l'application (données et procédure) => TMA
